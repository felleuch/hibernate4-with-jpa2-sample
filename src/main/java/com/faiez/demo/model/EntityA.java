
package com.faiez.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class EntityA
{
	//by default:
	// MySQL,MariaDB->id bigint auto_increment 
	//PostgreSQL->id bigint , and only one sequence called 'hibernate_sequence' for generating PKs
	//SQLSERVER 2012 id numeric(19,0) identity
	//ORACLE -> id number(19,0), and only one sequence called 'hibernate_sequence' for generatings PKs
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false, length=50)
	private String name;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "entityA")
	private List<EntityB> entities;


	public List<EntityB> getEntities()
	{
		return entities;
	}

	public void setEntities(List<EntityB> entities)
	{
		this.entities = entities;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}	
	
}